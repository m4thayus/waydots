#
# ~/.bash_profile
#
export PATH="$HOME/.node_modules/bin:$PATH"
export npm_config_prefix=~/.node_modules
export PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"
export PATH="$PATH:$HOME/.local/bin"

eval "$(rbenv init -)"

eval $(/usr/bin/gnome-keyring-daemon --start --components=gpg,pkcs11,secrets,ssh)
export GNOME_KEYRING_CONTROL GNOME_KEYRING_PID GPG_AGENT_INFO SSH_AUTH_SOCK

[[ -f ~/.bashrc ]] && . ~/.bashrc
