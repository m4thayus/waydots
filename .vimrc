set number
set relativenumber
set linebreak
set showbreak=+++
set textwidth=0
set showmatch

set hlsearch
set smartcase
set ignorecase
set incsearch
 
set autoindent
set expandtab
set shiftwidth=4
set softtabstop=4
 
set ruler
 
set undolevels=1000
set backspace=indent,eol,start

colorscheme desert
syntax on
