#!/bin/bash

stat() {
    cat /sys/class/power_supply/BAT0/status
}

cap() {
    cat /sys/class/power_supply/BAT0/capacity
}

energy_full() {
    cat /sys/class/power_supply/BAT0/energy_full
}

energy_now() {
    cat /sys/class/power_supply/BAT0/energy_now
}

power_now() {
    cat /sys/class/power_supply/BAT0/power_now
}

if [[ $(power_now) -eq 0 ]]; then 

    notify-send 'Battery Status' "Fully charged to $(cap)% of capacity.\nCurrently on AC power."

elif [[ $(stat) == "Charging" ]]; then 

    let time_charged=60\*$(( $(energy_full) - $(energy_now) ))/$(power_now)
    notify-send 'Battery Status' "$time_charged mins until charged."

else

    let time_empty=60\*$(energy_now)/$(power_now)
    notify-send 'Battery Status' "$(($time_empty/60))h $(($time_empty%60))m until depleted."

fi
