#!/bin/bash

function speed() {
    cat /sys/class/hwmon/hwmon4/fan1_input
}

function temp() {
    cat /sys/devices/platform/coretemp.0/hwmon/hwmon6/temp1_input
}

TEMP="$(temp)"
SPEED="$(speed)"

if [[ SPEED -eq 0 ]]; then 

    notify-send 'Thermal Status' "${TEMP::-3}°C on the CPU package.\nFans are idle."

else

    notify-send 'Thermal Status' "${TEMP::-3}°C on the CPU package.\nFans are spinning at ${SPEED} RPM."

fi
