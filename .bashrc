#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Various aliases
alias waydots='/usr/bin/git --git-dir=/home/matt/.waydots --work-tree=/home/matt'
alias be='bundle exec'

# Prompt Customization
function git_branch() {

    inside_git_repo="$(git rev-parse --is-inside-work-tree 2>/dev/null)"

    if [ "$inside_git_repo" ]; then
        echo "($(git branch --show-current)) "
    fi
}

RED="\[$(tput setaf 1)\]"
GREEN="\[$(tput setaf 2)\]"
YELLOW="\[$(tput setaf 3)\]"
BLUE="\[$(tput setaf 4)\]"
GREY="\[$(tput setaf 8)\]"
RESET="\[$(tput sgr0)\]"

if [ $(id -u) -eq 0 ];
then
    PS1="${RED}\u${RESET}@${GREY}\h: ${BLUE}\W ${YELLOW}\$(git_branch)${RESET}\$ "
else
    PS1="${GREEN}\u${RESET}@${GREY}\h: ${BLUE}\W ${YELLOW}\$(git_branch)${RESET}\$ "
fi

# VIM Keys
set -o vi

# Adjust columns based on window size
shopt -s checkwinsize

# If running from tty1 prompt to start a graphical session
if [ "$(tty)" = "/dev/tty1" ]; then
    echo "Choose a Session:"
    PS3='?:'
    sessions=("Sway (Wayland)" "i3 (X11)" "Exit")
    select opt in "${sessions[@]}"
    do
        case $opt in
            "Sway (Wayland)")
	        exec sway
                ;;
            "i3 (X11)")
                exec startx
                ;;
            "Exit")
                break
                ;;
            *) echo "invalid option $REPLY";;
        esac
    done
fi

# tabtab source for electron-forge package
# uninstall by removing these lines or running `tabtab uninstall electron-forge`
[ -f /home/matt/.config/yarn/global/node_modules/tabtab/.completions/electron-forge.bash ] && . /home/matt/.config/yarn/global/node_modules/tabtab/.completions/electron-forge.bash
